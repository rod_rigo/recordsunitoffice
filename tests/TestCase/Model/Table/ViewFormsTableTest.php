<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewFormsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewFormsTable Test Case
 */
class ViewFormsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewFormsTable
     */
    protected $ViewForms;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewForms',
        'app.Categories',
        'app.Forms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ViewForms') ? [] : ['className' => ViewFormsTable::class];
        $this->ViewForms = $this->getTableLocator()->get('ViewForms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewForms);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewFormsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewFormsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
