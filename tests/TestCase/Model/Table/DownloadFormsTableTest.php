<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DownloadFormsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DownloadFormsTable Test Case
 */
class DownloadFormsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DownloadFormsTable
     */
    protected $DownloadForms;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.DownloadForms',
        'app.Categories',
        'app.Forms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('DownloadForms') ? [] : ['className' => DownloadFormsTable::class];
        $this->DownloadForms = $this->getTableLocator()->get('DownloadForms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->DownloadForms);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\DownloadFormsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\DownloadFormsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
