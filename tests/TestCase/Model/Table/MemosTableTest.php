<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MemosTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MemosTable Test Case
 */
class MemosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MemosTable
     */
    protected $Memos;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Memos',
        'app.Users',
        'app.Documents',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Memos') ? [] : ['className' => MemosTable::class];
        $this->Memos = $this->getTableLocator()->get('Memos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Memos);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MemosTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\MemosTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
