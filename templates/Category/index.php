<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>

<script>
    var id = parseInt(<?=intval($id)?>);
    var text = '<?=strtoupper($category->category)?>';
</script>

<?=$this->Html->css('categories/index')?>

<div class="modal fade" id="modal" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <embed src="javascript:void(0);" id="embed" style="width: 100%;" height="800" title="">
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">

            </div>
        </div>
    </div>
</div>

<div class="p-sm-0 p-md-5 p-lg-5 mt-sm-1 mt-md-3 mt-lg-3">

</div>

<div class="container-fluid py-5 wow fadeInUp">
    <div class="container py-3">
        <div class="row g-5">

            <h1 class="mb-0 text-right" id="typed"></h1>
            <!-- Sidebar Start -->
            <div class="col-sm-12 col-md-5 col-lg-4 mt-2">
                <!-- Search Form Start -->
                <?=$this->Form->create(null,['type' => 'file', 'id' => 'form', 'class' => 'mb-5', 'method' => 'get'])?>
                <div class="input-group">
                    <?=$this->Form->search('search',[
                        'class' => 'form-control p-3',
                        'id' => 'search',
                        'placeholder' => ucwords('Search Here...'),
                        'required' => true,
                        'value' => $search
                    ])?>
                    <?=$this->Form->button('<i class="bi bi-search"></i>',[
                        'class' => 'btn btn-primary px-4',
                        'escapeTitle' => false,
                        'type' => 'submit'
                    ])?>
                </div>
                <?=$this->Form->end()?>
                <!-- Search Form End -->
            </div>
            <!-- Sidebar End -->

            <!-- Blog list Start -->
            <div class="col-sm-12 col-md-7 col-lg-8 mt-2 scroll">
                <div class="row m-0 p-0" id="form-list">

                    <?php if(intval(count($forms)) > intval(0)):?>
                        <?php foreach ($forms as $form):?>
                            <div class="col-sm-12 col-md-12 col-lg-12 m-2">
                                <div class="blog-item bg-light rounded overflow-hidden">
                                    <div class="p-4">
                                        <div class="d-flex mb-3">
                                            <small class="me-3">
                                                <i class="far fa-user text-primary me-2"></i>
                                                <?=$form['user']['username']?>
                                            </small>
                                            <small>
                                                <i class="far fa-calendar-alt text-primary me-2"></i>
                                                <?=date('Y-m-d', strtotime($form['modified']))?>
                                            </small>
                                        </div>
                                        <h5 class="mb-3"> <?=strtoupper($form['title'])?></h5>
                                        <a class="text-uppercase view btn btn-primary" href="javascript:void(0);" data-url="<?=$this->Url->assetUrl('/form/'.($form['form']))?>" data-id="<?=intval($form['id'])?>" title="<?=strtoupper($form['title'])?>" style="color: white !important;">
                                            View
                                            <?php if(intval(count($form['view_forms'])) > intval(0)):?>
                                                <span id="view-<?=intval($form['id'])?>" data-id="<?=(doubleval($form['view_forms'][0]['total']))?>">
                                                    (<?=number_format(doubleval($form['view_forms'][0]['total']))?>)
                                                </span>
                                            <?php else:?>
                                                <span id="view-<?=intval($form['id'])?>" data-id="0">

                                                </span>
                                            <?php endif;?>
                                            <i class="fa fa-bookmark"></i>
                                        </a>
                                        |
                                        <a class="text-uppercase download btn btn-primary" href="javascript:void(0);" data-id="<?=intval($form['id'])?>" title="<?=strtoupper($form['title'])?>" style="background: #ec3125 !important; border: #ec3125 !important; color: white !important;">
                                            Download
                                            <?php if(intval(count($form['download_forms'])) > intval(0)):?>
                                                <span id="download-<?=intval($form['id'])?>" data-id="<?=(doubleval($form['download_forms'][0]['total']))?>">
                                                    (<?=number_format(doubleval($form['download_forms'][0]['total']))?>)
                                                </span>
                                            <?php else:?>
                                                <span id="download-<?=intval($form['id'])?>" data-id="0">

                                                </span>
                                            <?php endif;?>
                                            <i class="fa fa-download"></i>
                                        </a>
                                        |
                                        <a class="text-uppercase copy btn btn-info" href="javascript:void(0);" data-id="<?=intval($form['id'])?>" title="Copy To Clipboard" style="color: #fff !important;;">
                                            Copy Link
                                            <i class="fa fa-link"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>
            </div>
            <!-- Blog list End -->

            <div class="col-sm-12 col-md-12 col-lg-12 mt-3 d-flex justify-content-end align-items-center">
                <div id="after-paginator"></div>
                <?php
                $this->Paginator->setTemplates([
                    'first' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>',
                    'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
                    'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
                    'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                    'ellipsis' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></li>',
                    'last' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>',
                    'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
                    'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
                ]);
                ?>
                <ul class="pagination d-flex justify-content-start align-items-center">
                    <?= $this->Paginator->first();?>
                    <?= $this->Paginator->prev();?>
                    <?= $this->Paginator->numbers();?>
                    <?= $this->Paginator->next();?>
                    <?= $this->Paginator->last();?>
                    <li class="page-item pageNumbers">
                        <a href="#" class="page-link">
                            <?= $this->Paginator->counter('Page {{page}}/{{pages}}'); ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('categories/index')?>


