<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DownloadForm[]|\Cake\Collection\CollectionInterface $downloadForms
 */
?>

<?=$this->Html->css('admin/download-forms/index')?>

<div class="row">
    <?=$this->Form->create(null,['id' => 'form', 'class' => 'col-sm-12 col-md-3 col-lg-2 mb-3', 'type' => 'file'])?>
    <?=$this->Form->label('year', ucwords('Year'))?>
    <div class="input-group ">
        <?=$this->Form->year('year',[
            'id' => 'year',
            'value' => intval(date('Y')),
            'min' => 2000,
            'class' => 'form-control form-control-border',
            'title' => ucwords('Year'),
            'required' => true,
        ])?>
        <span class="input-group-append">
                <?=$this->Form->button('Submit',[
                    'type' => 'submit',
                    'class' => 'btn btn-info btn-flat'
                ])?>
            </span>
    </div>
    <?=$this->Form->end()?>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Category</th>
                        <th>Form</th>
                        <th>Year</th>
                        <th>Month</th>
                        <th>Download</th>
                        <th>Modified</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/download-forms/index')?>

