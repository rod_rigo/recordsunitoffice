<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Form[]|\Cake\Collection\CollectionInterface $forms
 */
?>

<?=$this->Html->css('admin/forms/week')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Forms', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="New Form">
            New Form
        </a>
    </div>

    <div class="col-sm-12 col-md-5 col-lg-4 mb-3">
        <?=$this->Form->label('category', ucwords('category'))?>
        <?=$this->Form->select('category', $categories,[
            'class' => 'form-control rounded-0',
            'id' => 'category',
            'required' => true,
            'empty' => ucwords('select category'),
            'title' => ucwords('Please Fill Out This Field')
        ])?>
        <small></small>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Is Published</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/forms/week')?>
