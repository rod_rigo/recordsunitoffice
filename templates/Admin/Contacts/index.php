<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact[]|\Cake\Collection\CollectionInterface $contacts
 */
?>

<div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Contacts', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="New Contact">
                New Contact
            </a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card p-3">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Header</th>
                            <th>Title</th>
                            <th>Address</th>
                            <th>Contact/Tel</th>
                            <th>Email</th>
                            <th>Description</th>
                            <th>Is Active</th>
                            <th>Modified By</th>
                            <th>Modified</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?=$this->Html->script('admin/contacts/index')?>