<?php
/**
 * @var \App\View\AppView $this
 */

$memos = \Cake\ORM\TableRegistry::getTableLocator()->get('Memos')
    ->find()
    ->contain([
        'Documents' => [
            'queryBuilder' => function($query){
                return $query->find('all')
                    ->select([
                        'year',
                        'memo_id'
                    ])
                    ->group([
                        'year',
                        'memo_id'
                    ])
                    ->order([
                        'Documents.year' => 'ASC'
                    ], true);
            }
        ]
    ])
    ->where([
        'Memos.is_shown =' => intval(1)
    ])
    ->order(['Memos.memo' => 'ASC'],true);

$categories = \Cake\ORM\TableRegistry::getTableLocator()->get('Categories')
    ->find()
    ->where([
        'Categories.is_shown =' => intval(1)
    ])
    ->order(['Categories.category' => 'ASC'],true);
?>

<div class="container-fluid position-relative p-0">
    <nav class="navbar navbar-expand-lg navbar-dark px-5 py-3 py-lg-0">
        <a href="javascript:void(0);" class="navbar-brand p-0 d-flex flex-wrap">
            <img src="<?=$this->Url->assetUrl('/img/do-logo.png')?>" class="w-25" height="80" width="80" style="object-fit: contain !important;">
            <h5 class="m-0 w-75 d-flex justify-content-center align-items-center">
                Schools Division Office <br> Of Santiago City
            </h5>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto py-0">
                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Websites', 'action' => 'index'])?>" turbolink class="nav-item nav-link mx-3">Home</a>
                <?php if(!$memos->isEmpty()):?>
                    <div class="nav-item dropdown mx-3">
                        <a href="javascript:void(0);" class="nav-link dropdown-toggle ml-0" data-bs-toggle="dropdown">Memo</a>
                        <div class="dropdown-menu m-0">
                            <?php foreach($memos as $memo):?>
                                <a class="dropdown-item dropdown-item-sub-menu" style="cursor: pointer !important;" data-id="<?=intval($memo->id)?>" data-bs-toggle="dropdown">
                                    <i class="fa fa-chevron-right m-2" id="chevron-<?=intval($memo->id)?>"></i><?=ucwords($memo->memo)?>
                                </a>
                                <?php if(count($memo->documents)):?>
                                    <div class="dropdown-submenu border-bottom d-none">
                                        <?php foreach ($memo->documents as $document):?>
                                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Memos', 'action' => 'index', intval($memo->id), intval($document->year), '?' => ['query' => time()]])?>"  class="dropdown-item" style="cursor: pointer !important; text-indent: 10px !important;" turbolink>
                                                <i class="fa fa-chevron-right ml-2 mr-1" style="margin-right: 5px !important;"></i><?=intval($document->year)?>
                                            </a>
                                        <?php endforeach;?>
                                    </div>
                                <?php endif;?>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>
                <?php if(!$categories->isEmpty()):?>
                    <div class="nav-item dropdown mx-3">
                        <a href="javascript:void(0);" class="nav-link dropdown-toggle ml-0" data-bs-toggle="dropdown">Downloadable Forms</a>
                        <div class="dropdown-menu m-0">
                            <?php foreach($categories as $category):?>
                                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Category', 'action' => 'index', intval($category->id), '?' => ['query' => time()]])?>" turbolink class="dropdown-item"><?=ucwords($category->category)?></a>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>
                <?php if(strtolower($controller) === strtolower('websites')):?>
                    <a href="javascript:void(0);" class="nav-item nav-link"><i class="fa fa-minus fa-rotate-90 mx-3"></i></a>
                    <a href="javascript:void(0);" data-target="#team" class="nav-item nav-link scroll-link mx-3">Team</a>
                    <a href="javascript:void(0);" data-target="#about" class="nav-item nav-link scroll-link mx-3">About</a>
                    <a href="javascript:void(0);" data-target="#contact" class="nav-item nav-link scroll-link mx-3">Contact</a>
                <?php endif;?>
                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'login'])?>" turbolink class="nav-item nav-link  mx-3">
                    Login
                </a>
            </div>
        </div>
    </nav>
</div>
