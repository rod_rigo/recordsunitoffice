<?php
/**
 * @var \App\View\AppView $this
 */

$contact = \Cake\ORM\TableRegistry::getTableLocator()->get('Contacts')
    ->find()
    ->where([
        'is_active =' => intval(1)
    ])
    ->first();

?>

<div class="container-fluid bg-dark px-5 d-none d-lg-block">
    <div class="row gx-0">
        <div class="col-lg-12 text-center text-lg-start mb-2 mb-lg-0">
            <div class="d-inline-flex align-items-center" style="height: 45px;">
                <?php if(!empty($contact)):?>
                    <small class="me-3 text-light">
                        <i class="fa fa-map-marker-alt me-2"></i>
                        <?=ucwords($contact->address)?>
                    </small>
                    <small class="me-3 text-light">
                        <i class="fa fa-phone-alt me-2"></i>
                        <?=$contact->contact_mobile?>
                    </small>
                    <small class="text-light">
                        <i class="fa fa-envelope-open me-2"></i>
                        <?=$contact->email?>
                    </small>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
