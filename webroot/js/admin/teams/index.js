'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'teams/';
    var url = '';
    var src = $('#image-preview').attr('src');
    var isActive = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getTeams',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(5)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1+'<input type="hidden" class="order-positions" value="'+(parseInt(row+1))+'"  data-id="'+(parseInt(full.id))+'" readonly required/>';
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return '<img src="/recordsunitoffice/img/'+(row.team_image)+'" loading="lazy" height="100" width="100">';
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [7],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'name'},
            { data: 'position'},
            { data: 'team_image'},
            { data: 'is_active'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.edit',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'edit/'+(parseInt(dataId));
        $.ajax({
            url:href,
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                url = 'edit/'+(parseInt(dataId));
                $('#file').removeAttr('required');
                $('button[type="reset"]').fadeOut(100);
            },
        }).done(function (data, status, xhr) {
            $('#name').val(data.name);
            $('#position').val(data.position);
            $('#team-image').val(data.team_image);
            $('#is-active').val(data.is_active);
            $('#active').prop('checked', data.is_active);
            $('#order-position').val(data.order_position);
            $('#image-preview').attr('src', '/recordsunitoffice/img/'+(data.team_image));
            $('#modal').modal('toggle');
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });

    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    datatable.on('click','img',function (e) {
        var src = $(this).attr('src');
        window.open(src);
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl + url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('#modal').modal('toggle');
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#toggle-modal').click(function (e) {
        url = 'add';
        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#form')[0].reset();
        $('small').empty();
        $('#image-preview').attr('src', src);
        $('.form-control').removeClass('is-invalid');
        $('#file').prop('required', true);
        $('button[type="reset"]').fadeIn(100);
        $('button[type="reset"], button[type="submit"]').prop('disabled', false);
    });

    $('#name').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#position').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#file').change(function (e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        var mimes = [
            'image/png',
            'image/jfif',
            'image/jpg',
            'image/jpeg',
            'image/webp',
        ];

        if(!mimes.includes((file.type).toLowerCase())){
            $(this).addClass('is-invalid').next('small').text('Only PNG, JFIF, JPG, JPEG, & WEBP Format Is Allowed');
            return true;
        }

        reader.addEventListener('load', function (e) {
            $('#image-preview').attr('src', e.target.result);
        });

        reader.readAsDataURL(file);
        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

    $(document).find('#datatable tbody').sortable({
        revert: true,
        handle: 'td:nth-child(1)',
        start:function( event, ui ){

        },
        update: function( event, ui ) {
            console.log(table.data().toArray());
            positions();
        },
    });

    function positions() {
        var data = [];
        var counter = 0;
        $('.order-positions').each(function () {
            var dataId = $(this).attr('data-id');
            counter++;
            data.push({
                id : parseInt(dataId),
                order_position : parseInt(counter)
            });
        });
        $.ajax({
            url: baseurl+'orderPositions',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json', // Set content type to JSON
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            table.ajax.reload(null, false);
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});