Turbolinks.start();
$('a[turbolink]').click(function (e) {
    var href = $(this).attr('href');
    Turbolinks.visit(href,{action:'replace'});
});

$('a.dropdown-item-sub-menu').click(function () {

    var dataId = $(this).attr('data-id');
    if($(this).hasClass('active')){
        $('#chevron-'+(dataId)+'').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        $(this).removeClass('active');
    }else{
        $(this).addClass('active');
        $('#chevron-'+(dataId)+'').removeClass('fa-chevron-right').addClass('fa-chevron-down');
    }

    var element = $(this).next('.dropdown-submenu');
    if(element.hasClass('d-none')){
        element.removeClass('d-none').slideDown(1000);
    }else{
        element.addClass('d-none').slideUp(1000);
    }

    $('.dropdown-item-sub-menu').not(this).removeClass('active');
}).blur(function () {
    var dataId = $(this).attr('data-id');
    $(this).removeClass('active');
    $('#chevron-'+(dataId)+'').removeClass('fa-chevron-right').addClass('fa-chevron-down');
});
