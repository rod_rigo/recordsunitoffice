'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'form/getForms/'+(id)+'?search=';

    var typed = new Typed('#typed', {
        strings: [(text).toUpperCase()],
        startDelay: 1000,
        typeSpeed: 50,
        backSpeed: 0,
        cursorChar: '',
        smartBackspace: true,
        fadeOut: true,
        loop: false,
    });

    $('#search').on('input', function (e) {
        $('button[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i>');
        setTimeout(function () {
            getForms();
        }, 800);
    });

    $(document).on('click', '.view', function (e) {
        e.preventDefault();
        var dataUrl = $(this).attr('data-url');
        var title = $(this).attr('title');
        var dataId = $(this).attr('data-id');
        var date = new Date();
        $.ajax({
            url: (dataUrl)+'?timestamp='+(date.getMilliseconds()),
            type: 'HEAD',
            method: 'HEAD',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $('#embed').attr('src', (dataUrl)+'?timestamp='+(date.getMilliseconds())).attr('title', title);
            $('#modal').modal('toggle');
            if(storage(parseInt(dataId))){
                view(parseInt(dataId));
            }
        }).fail(function (data, status, xhr) {
            swal('error', null, 'File Not Found');
        });
    });

    $(document).on('click', '.download', function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var downloads = $('#download-'+(dataId)+'').attr('data-id');
        var href = mainurl+'form/download/'+(parseInt(dataId));
        Swal.fire({
            title: 'Download File',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                downloads++;
                $('#download-'+(dataId)+'').attr('data-id', parseFloat(downloads)).text('('+(new Intl.NumberFormat().format(parseFloat(downloads)))+')');
                window.open(href);
            }
        });
    });

    $(document).on('click', '.copy', function (e) {
        var dataId = $(this).attr('data-id');
        var href = mainurl +'category/index/'+(id)+'?form_id='+(dataId);
        navigator.clipboard.writeText(href)
            .then(function() {
                Swal.fire({
                    icon: 'success',
                    title: 'Text Copied To Clipboard Successfully',
                    text: null,
                    timer: 1500,
                    timerProgressBar:true,
                    showConfirmButton:false,
                    toast:true,
                    position:'top-right'
                });
            }).catch(function(e) {

        });
    });

    function getForms() {
        $.ajax({
            url: baseurl+($('#search').val()),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('button[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i>');
            },
        }).done(function (data, status, xhr) {
            $('button[type="submit"]').html('<i class="bi bi-search"></i>');
            var html = '';
            $.map(data, function (data, key) {
                var download = '';
                if(parseInt((data.download_forms).length) > parseInt(0)){
                    download = ' <span id="download-'+(parseInt(data.id))+'" data-id="'+(parseFloat(data.download_forms[0].total))+'">('+(new Intl.NumberFormat().format(parseFloat(data.download_forms[0].total)))+')</span>';
                }else{
                    download = '<span id="download-'+(parseInt(data.id))+'" data-id="0"></span> ';
                }

                var view = '';
                if(parseInt((data.view_forms).length) > parseInt(0)){
                    view = ' <span id="view-'+(parseInt(data.id))+'" data-id="'+(parseFloat(data.view_forms[0].total))+'">('+(new Intl.NumberFormat().format(parseFloat(data.view_forms[0].total)))+')</span>';
                }else{
                    view = '<span id="view-'+(parseInt(data.id))+'" data-id="0"></span> ';
                }

                html += '<div class="cpl-sm-12 col-md-12 col-lg-12 m-2">' +
                    '<div class="blog-item bg-light rounded overflow-hidden">'+
                    '<div class="p-4">'+
                    '<div class="d-flex mb-3">'+
                    '<small class="me-3">'+
                    '<i class="far fa-user text-primary me-2"></i>'+
                    (data.user.username)+
                    '</small>'+
                    '<small>'+
                    '<i class="far fa-calendar-alt text-primary me-2"></i>'+
                    (moment(data.modified).format('Y-MM-DD'))+
                    '</small>'+
                    '</div>'+
                    '<h5 class="mb-3">'+(data.title)+'</h5>'+
                    '<a class="text-uppercase view btn btn-primary" href="javascript:void(0);" data-url="/recordsunitoffice/form/'+(data.form)+'" title="'+((data.title).toUpperCase())+'" data-id="'+(parseInt(data.id))+'" style="color: white !important;">' +
                    'View ' +
                    (view) +
                    '<i class="fa fa-bookmark"></i></a>' +
                    ' | '+
                    '<a class="text-uppercase download btn btn-primary" href="javascript:void(0);" data-id="'+(parseInt(data.id))+'" title="'+((data.title).toUpperCase())+'" style="background: #ec3125 !important; border: #ec3125 !important; color: white !important;"> ' +
                    'Download ' +
                    (download) +
                    '<i class="fa fa-download"></i> </a>' +
                    ' | '+
                    '<a class="text-uppercase copy btn btn-info" href="javascript:void(0);" data-id="'+(parseInt(data.id))+'" title="Copy To Clipboard" style="color: #fff !important;;">'+
                    'Copy Link'+
                    '<i class="fa fa-link"></i>'+
                    '</a>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
            });
            $('#form-list').html(html);
        }).fail(function (data, status, xhr) {

        }).always(function (data, status, xhr) {
            $('#after-paginator').next().remove();
            $('#form-list').paginathing({
                perPage: 25,
                limitPagination: false,
                containerClass:'d-flex justify-content-center justify-content-center flex-wrap mt-2',
                ulClass: 'pagination',
                prevNext: true,
                firstLast: true,
                prevText: '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                nextText: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
                firstText: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                lastText: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                liClass: 'page-item',
                activeClass: 'active',
                disabledClass: 'disabled',
                insertAfter:'#after-paginator',
                pageNumbers: true,
            });
        });
    }

    function view(documentId) {
        var views = $('#view-'+(documentId)+'').attr('data-id');
        $.ajax({
            url:mainurl+'form/view/'+(documentId),
            type: 'POST',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:'JSON',
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            views++;
            $('#view-'+(documentId)+'').attr('data-id', parseFloat(views)).text('('+(new Intl.NumberFormat().format(parseFloat(views)))+')');
        }).fail(function (data, status, xhr) {

        });
    }

    function storage(documentId) {
        var objects = '';
        var is_added = false;
        var forms = localStorage.getItem('forms');
        var month = moment().format('MMM');
        var year = moment().format('Y');
        if(forms){

            objects = JSON.parse(forms);

            if(!objects.hasOwnProperty(parseInt(year))){
                objects = {
                    [year.toString()]:{}
                };
            }

            var keys = Object.keys(objects[year]);

            if(!keys.includes(month)){
                objects[year][month] = [];
            }

            var values = Object.values(objects[year][month]);

            if(!values.includes(parseInt(documentId))){
                objects[year][month].push(parseInt(documentId));
                is_added = true;
            }

            objects = JSON.stringify(objects);

            localStorage.setItem('forms', objects);
        }else{
            objects = {
                [year.toString()]:{

                }
            };
            objects[year][month] = [parseInt(documentId)];
            objects = JSON.stringify(objects);
            localStorage.setItem('forms', objects);
            is_added = true;
        }

        return is_added;
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        });
    }

});