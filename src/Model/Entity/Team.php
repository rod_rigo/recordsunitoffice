<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Team Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $team_image
 * @property string $position
 * @property int $is_active
 * @property int $order_position
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 */
class Team extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'team_image' => true,
        'position' => true,
        'is_active' => true,
        'order_position' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
    ];

    protected function _setName($value){
        return strtoupper($value);
    }

    protected function _setPosition($value){
        return strtoupper($value);
    }

}
