<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $header
 * @property string $title
 * @property string $address
 * @property float $latitude
 * @property float $longtitude
 * @property string $contact_mobile
 * @property string $email
 * @property string $description
 * @property string $content
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 */
class Contact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'header' => true,
        'title' => true,
        'address' => true,
        'latitude' => true,
        'longtitude' => true,
        'contact_mobile' => true,
        'email' => true,
        'description' => true,
        'content' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
    ];

    public function _setHeader($value){
        return strtoupper($value);
    }

    public function _setTitle($value){
        return strtoupper($value);
    }

    public function _setAddress($value){
        return ucwords($value);
    }

    public function _setDescription($value){
        return strtoupper($value);
    }

}
