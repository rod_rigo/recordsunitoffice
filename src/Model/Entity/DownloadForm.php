<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DownloadForm Entity
 *
 * @property int $id
 * @property int $category_id
 * @property int $form_id
 * @property string $year
 * @property string $month
 * @property float $download_form
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Form $form
 */
class DownloadForm extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category_id' => true,
        'form_id' => true,
        'year' => true,
        'month' => true,
        'download_form' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'category' => true,
        'form' => true,
    ];

    protected function _setMonth($value){
        return strtoupper($value);
    }

}
