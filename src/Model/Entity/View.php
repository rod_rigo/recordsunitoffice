<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * View Entity
 *
 * @property int $id
 * @property int $memo_id
 * @property int $document_id
 * @property string $year
 * @property string $month
 * @property float $view
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Memo $memo
 * @property \App\Model\Entity\Document $document
 */
class View extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'memo_id' => true,
        'document_id' => true,
        'year' => true,
        'month' => true,
        'view' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'memo' => true,
        'document' => true,
    ];

    protected function _setMonth($value){
        return strtoupper($value);
    }


}
